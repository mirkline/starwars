import {RootState} from "../rootReducer";

export const starshipSelector = ( state: RootState ) => state.starships;