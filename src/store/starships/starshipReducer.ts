import { StarshipAction, StarshipsActionTypes, StarshipsState} from '../../types/starships';

export const initialState: StarshipsState  = {
    starships: [],
    count: 1,
    isLoadingStarships: false,
    currentPage: 1,
    error: '',
    modal: {
        isModal: false,
        id: null,
        url: null,
        starship: null
    }
}

export const starshipReducer = (state = initialState, action: StarshipAction): StarshipsState => {
    switch (action.type) {
        case StarshipsActionTypes.IS_LOADING_STARSHIPS:
            return { ...state, isLoadingStarships: action.payload, error: null}
        case StarshipsActionTypes.GET_STARSHIPS:
            return { ...state, starships: action.payload}
        case StarshipsActionTypes.ADD_STARSHIPS:
            return { ...state, starships: [...state.starships, ...action.payload]}
        case StarshipsActionTypes.LOADING_STARSHIPS_ERROR:
            return { ...state, isLoadingStarships: false, error: action.payload}
        case StarshipsActionTypes.CURRENT_PAGE:
            return { ...state, currentPage: action.payload}
        case StarshipsActionTypes.MODAL:
            return {...state, modal: {id: action.payload.id, isModal: action.payload.isModal, url: action.payload.url, starship: action.payload.starship}}
        case StarshipsActionTypes.COUNT:
            return { ...state, count: action.payload}
        default:
            return state
    }
}