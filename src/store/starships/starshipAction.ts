import axios from "axios";
import {IModal, IStarship, StarshipAction, StarshipsActionTypes} from "../../types/starships";
import {Dispatch} from "redux";
import {starshipSelector} from "./starshipSelectors";
import {STARSHIPS_URL} from "../../utils/api";

export const addStarships = (payload: IStarship[]): StarshipAction => ({type: StarshipsActionTypes.GET_STARSHIPS, payload})
export const upStarships = (payload: IStarship[]): StarshipAction => ({type: StarshipsActionTypes.ADD_STARSHIPS, payload})
export const loadingStarships = (payload: boolean): StarshipAction => ({type: StarshipsActionTypes.IS_LOADING_STARSHIPS, payload})
export const errorStarships = (payload: string): StarshipAction => ({type: StarshipsActionTypes.LOADING_STARSHIPS_ERROR, payload})
export const changePage = (payload: number): StarshipAction => ({type: StarshipsActionTypes.CURRENT_PAGE, payload})
export const changeCount = (payload: number): StarshipAction => ({type: StarshipsActionTypes.COUNT, payload})
export const controlModal = (payload: IModal): StarshipAction => ({type: StarshipsActionTypes.MODAL, payload})

export const getStarships = (searchValue: string, shouldResetPage = false) => {
    return async (dispatch: Dispatch<StarshipAction>, getState: any) => {
        const state = getState();
        let {currentPage, isLoadingStarships, count}= starshipSelector(state)
        if(isLoadingStarships) {
            return
        }
        if(shouldResetPage){
            dispatch(changePage(1));
            currentPage = 1;
        }
        const pageNumber = Math.ceil(count/10) || 1
        try {
            if( currentPage <=  pageNumber) {
                dispatch(loadingStarships(true))
                const response = await axios.get(searchValue ? `${STARSHIPS_URL}?search=${searchValue}&page=${currentPage}` : `${STARSHIPS_URL}?page=${currentPage}`)
                dispatch(loadingStarships(false))

                response.data.results.forEach((e: any) => {
                    e['id'] = Number(e.url.split('/').reverse()[1])
                })
                dispatch(changeCount(response.data.count))
                if(currentPage === 1){
                    dispatch(addStarships(response.data.results))
                }else {
                    dispatch(upStarships(response.data.results))
                }
                !shouldResetPage && dispatch(changePage(currentPage + 1))
            }
        } catch (e) {
            dispatch(errorStarships('Произошла ошибка'))
        }
    }
}
