import {combineReducers} from "redux";
import { starshipReducer } from './starships/starshipReducer'


export const rootReducer = combineReducers({
  starships: starshipReducer
})

export type RootState = ReturnType<typeof  rootReducer>