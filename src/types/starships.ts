export interface StarshipsState {
    starships: IStarship[];
    count: number;
    isLoadingStarships: boolean;
    currentPage: number;
    modal : IModal;
    error: null | string;
}

export interface IModal {
    isModal: boolean;
    id: number | null;
    url: string | null,
    starship: IStarship | null
}

export enum StarshipsActionTypes {
    GET_STARSHIPS = 'GET_STARSHIPS',
    ADD_STARSHIPS = 'ADD_STARSHIPS',
    IS_LOADING_STARSHIPS = 'IS_LOADING_STARSHIPS',
    LOADING_STARSHIPS_ERROR = 'LOADING_STARSHIPS_ERROR',
    CURRENT_PAGE = 'CURRENT_PAGE',
    COUNT = 'COUNT',
    MODAL = 'MODAL'
}

interface GetStarshipsAction {
    type:StarshipsActionTypes.GET_STARSHIPS;
    payload: IStarship[];
}

interface AddStarshipsAction {
    type:StarshipsActionTypes.ADD_STARSHIPS;
    payload: IStarship[];
}

interface IsLoadingStarshipsAction {
    type:StarshipsActionTypes.IS_LOADING_STARSHIPS;
    payload: boolean;
}

interface PageAction {
    type:StarshipsActionTypes.CURRENT_PAGE;
    payload: number;
}

interface CountAction {
    type: StarshipsActionTypes.COUNT
    payload: number;
}

interface ModalAction {
    type: StarshipsActionTypes.MODAL
    payload: IModal
}

interface LoadingStarshipsErrorAction {
    type:StarshipsActionTypes.LOADING_STARSHIPS_ERROR;
    payload: string
}



export type StarshipAction = GetStarshipsAction | AddStarshipsAction | IsLoadingStarshipsAction | LoadingStarshipsErrorAction | PageAction | CountAction | ModalAction

export interface IStarship {
    name: string;
    model: string;
    manufacturer: string;
    cost_in_credits: string;
    length: string;
    max_atmosphering_speed: string;
    crew: string;
    passengers: string;
    cargo_capacity: string;
    consumables: string;
    hyperdrive_rating: string;
    MGLT: string;
    starship_class: string;
    pilots: any[];
    films: any[];
    created: string;
    edited: string;
    url: string;
    id: number
}
