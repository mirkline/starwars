import { useCallback, useRef} from 'react'

export const useDebounce = (func: Function, delay = 400) => {
  let debounce = useRef< NodeJS.Timeout | undefined>()
  return useCallback(
      (...args: any[]) => {
          clearTimeout(debounce.current)
          debounce.current = setTimeout(() => {
        func.apply(this, args);
      }, delay)
    },[func],
  )
}