import React, {FC} from 'react';
import './ItemContainer.css';
import {IStarship} from "../../types/starships";
import {useActions} from "../../hooks/useActions";

interface StarshipItemProps {
    starship: IStarship;
}

const ItemContainer: FC<StarshipItemProps> = ({starship}) => {
    const { controlModal } = useActions();

    const openModal = () => {
        controlModal({
            isModal: true,
            id: starship.id,
            url: starship.url,
            starship: starship
        });
    }

  return (
    <div className='item' onClick={openModal}>
      <h2 className='itemName'>{starship.name}</h2>
        <div className='itemInfoWrapper'>
          <div className='itemLength'>{starship.length}<span>Length</span></div>
          <div className='itemPrice'>{starship.cargo_capacity}<span>Cargo capacity</span></div>
        </div>
        <div className='itemType'>{starship.starship_class}</div>
    </div>
  );
};

export { ItemContainer };