import React, {FC} from 'react';
import {Routes, Route, BrowserRouter} from 'react-router-dom';
import { HomePage }  from './homePage/HomePage';
import { Starships } from './starships/Starships';
import { Header } from './header/Header';
import './App.css';

const App: FC = () => {

  return (
    <BrowserRouter>
        <Header />
        <Routes>
          <Route path='/' element={<HomePage />}/>
          <Route path='/starships' element={<Starships />}/>
        </Routes>
    </BrowserRouter>
  );
};

export { App };
