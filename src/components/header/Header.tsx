import React, {FC} from 'react';
import { NavLink, Link } from 'react-router-dom';
import './header.css';
import logo from '../../images/Logo.png'

const Header: FC = () => {
  
  return (
    <header className='header'>
      <div className='logo'>
        <Link to='/'>
          <img src={logo} alt='Star Wars' />
        </Link>
      </div>
      <ul className='menu'>
        <li>
          <NavLink to='/' end >Home</NavLink>
        </li>
        <li>
          <NavLink to='/starships'>Starships</NavLink>
        </li>
      </ul>
    </header>
  );
};

export { Header };