import React, {FC, useEffect, useState} from 'react';
import { useActions } from "../../hooks/useActions";
import {useDebounce} from "../../hooks/useDebounce";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { starshipSelector } from '../../store/starships/starshipSelectors';
import { ItemContainer } from '../itemContainer/ItemContainer';
import { Modal } from '../modal/Modal';
import './Starships.css';
import {Spinner} from "../spinner/Spinner";

const Starships: FC = () => {
    const { isLoadingStarships, starships, count } = useTypedSelector(starshipSelector);
    const { getStarships } = useActions();
    const [searchValue, setSearchValue] = useState('');
    const [endScroll, setEndScroll] = useState(false);

    const debounce = useDebounce(getStarships, 500);

    useEffect(() => {
        if(endScroll){
            getStarships(searchValue, false)
        }
    },[endScroll])

    useEffect(() => {
        debounce(searchValue, true)
    },[searchValue])

    useEffect(() => {
        const handleScroll = (e: React.SyntheticEvent<Document>) => {
            setEndScroll(e.currentTarget.documentElement.scrollHeight - (e.currentTarget.documentElement.scrollTop + window.innerHeight) < 50)
        }
        document.addEventListener('scroll', handleScroll as unknown as EventListener)
        return () => {
            document.removeEventListener('scroll', handleScroll as unknown as EventListener)
        };
    }, [])

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchValue(e.target.value)
    }

    return (
    <div className='starships'>
        <h1>{count} Starships for you to choose your favorite</h1>
        <form action="">
          <input
            type="text"
            placeholder='find...'
            className='searchInput'
            value={searchValue}
            onChange={handleChange}
          />
        </form>
        <div className='itemContainer'>
          {
            starships.map((starship) =>{
              return (
                <ItemContainer key={starship.id} starship={starship}/>
              )
            })
          }
        </div>
        {isLoadingStarships && <Spinner />}
        <Modal />
    </div>
    );
};

export { Starships };