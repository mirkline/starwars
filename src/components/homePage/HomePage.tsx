import React from 'react';
import { Link } from 'react-router-dom';
import './homePage.css';
import banner from '../../images/BannerComplete.png'

const HomePage = () => {
  
  return (
    <div className='homePage'>
      <div className='homeInfo'>
        <h1 className='homeTitle'>
        Find all your favorite starships
        </h1>
        <h2>
        You can find out all the information about your favorite starships
        </h2>
        <Link to='/starships'><button>See more...</button></Link>
      </div>
      <img src={banner} alt="Banner" className='banner'/>
    </div>
  );
};

export { HomePage } ;