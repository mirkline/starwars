import React, {FC} from 'react';
import spinner from '../../images/Spinner.svg';
import './spinner.css'

const Spinner: FC = () => {
    return (
        <div>
            <img
                src={spinner}
                className='spinner'
                alt="Loading..."
            />
        </div>
    );
}

export { Spinner }