import React from 'react';
import { starshipSelector } from '../../store/starships/starshipSelectors';
import './Modal.css';
import {useTypedSelector} from "../../hooks/useTypedSelector";
import {useActions} from "../../hooks/useActions";

const Modal: React.FC = () => {
    const { controlModal } = useActions();
    const { modal } = useTypedSelector(starshipSelector);
    const closedModal = () => {
      controlModal({
          isModal: false,
          id: null,
          url: null,
          starship: null
      });
    }
    if(!modal.isModal){
        return null
    }
    console.log(modal.starship)
    return (
        <div className='modal' onClick={closedModal}>
            <div className='modalContent' onClick={e => e.stopPropagation()}>
                <div className='contentImage'>
                <img src={ `https://starwars-visualguide.com/assets/img/starships/${modal.id}.jpg` }
                     onError={(event) => { event.currentTarget.src = 'https://starwars-visualguide.com/assets/img/big-placeholder.jpg'}}
                     alt=""
                />
            </div>
            <div className='contentDescription'>
                <h2 className='itemName'>{modal.starship?.name}</h2>
                <div className='itemInfoWrapper'>
                    <div className='itemLength'>{modal.starship?.length}<span>Length</span></div>
                    <div className='itemPrice'>{modal.starship?.cargo_capacity}<span>Cargo capacity</span></div>
                    <div className='itemMGLT'>{modal.starship?.MGLT}<span>MGLT</span></div>
                    <div className='itemCrew'>{modal.starship?.crew}<span>Crew</span></div>
                    <div className='itemMaxSpeed'>{modal.starship?.max_atmosphering_speed}
                        <span>Max speed</span>
                    </div>
                    <div className='itemPassengers'>{modal.starship?.passengers}<span>Passengers</span></div>
                </div>
                <div className='itemType'>{modal.starship?.starship_class}</div>
              </div>
            </div>
        </div>
    );
};

export { Modal };