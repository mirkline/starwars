import React from 'react';
import ReactDOM from 'react-dom/client';
import 'reset-css';
import {App} from './components/App';
import {Provider} from "react-redux";
import {store} from "./store";

const container = document.createElement('div');
container.id = 'root';

const root = ReactDOM.createRoot(container);
document.body.appendChild(container);
root.render(
    <Provider store={store}>
        <App />
    </Provider>
);
